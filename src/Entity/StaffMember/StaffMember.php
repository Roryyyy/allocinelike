<?php

namespace App\Entity\StaffMember;
use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
Abstract class StaffMember
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    protected string $firstName;

    #[ORM\Column(type: 'string')]
    protected ?string $lastName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }


    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }


    public function getLastName(): ?string
    {
        return $this->lastName;
    }


    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }
}