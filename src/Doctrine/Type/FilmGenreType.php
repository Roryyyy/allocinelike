<?php

namespace App\Doctrine\Type;

use App\Enum\FilmGenre;

class FilmGenreType extends AbstractEnumType
{

    const NAME = 'film_genre';

    public static function getEnumsClass(): string
    {
        return FilmGenre::class;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}