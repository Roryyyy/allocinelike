<?php

namespace App\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\Type;

/**
 * Class AbstractEnumType
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractEnumType extends Type
{
    abstract public static function getEnumsClass(): string;

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        if ($this->isInt()) {
            return $platform->getIntegerTypeDeclarationSQL(array_merge($column, [
                'type' => IntegerType::class,
            ]));
        }


        throw new \RuntimeException('Not implemented.');
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof \BackedEnum) {
            return $value->value;
        }

        return null;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $this->doGetEnumsClass()::tryFrom($value);
    }

    /**
     * Override it to false to set type to string instead of int.
     */
    protected function isInt(): bool
    {
        return true;
    }

    /**
     * @return \BackedEnum
     */
    private function doGetEnumsClass(): string
    {
        if (!enum_exists($enum = $this->getEnumsClass(), true)) {
            throw new \LogicException(sprintf(
                'The class given for "%s" must be an enum.',
                $this::class,
            ));
        }

        return $enum;
    }
}
