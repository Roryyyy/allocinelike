<?php

namespace App\Enum;

enum FilmGenre: string
{

    case ACTION = "Action";
    case THRILLER = "Thriller";
    case HORROR = "Horror";
    case DRAMA = 'Drama';
    case ADVENTURE = 'Adventure';
    case COMEDY = 'Comedy';
    case FANTASTIC = 'Fantastic';
    case SCIFI = 'Sci-fi';
    case WESTERN = 'Western';
    case ANIMATION = 'Animation Film';

}